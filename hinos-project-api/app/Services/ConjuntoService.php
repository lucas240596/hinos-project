<?php

namespace App\Services;

use App\Repositories\ConjuntoRepository;
use App\Validators\ConjuntoValidator;
use Illuminate\Http\Request;
use Exception;

class ConjuntoService extends Services
{
    private $conjuntoRepository;

    public function __construct()
    {
        $this->conjuntoRepository = new ConjuntoRepository();
    }

    public function getAll()
    {
        try
        {
            $conjuntos = $this->conjuntoRepository->getAll();
            return $this->responseJsonData($conjuntos);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function get(int $id)
    {
        try
        {
            $conjunto = $this->conjuntoRepository->get($id);
            return $this->responseJsonData($conjunto);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function create(Request $request)
    {
        $validator = $this->validate($request->all(), ConjuntoValidator::CREATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            if($this->conjuntoRepository->getByName($request->input('nome')))
                return $this->responseError('Conjunto já existe', 409);

            $params = array(
                'nome' => $request->input('nome'),
            );

            $this->conjuntoRepository->create($params);
            return $this->responseJsonMessage('Cadastrado com sucesso', 201);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function update(int $id, Request $request)
    {
        $validator = $this->validate($request->all(), ConjuntoValidator::UPDATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            if(empty($this->conjuntoRepository->get($id)))
                return $this->responseError('Conjunto não encontrado', 404);

            $params = array(
                'nome'  => $request->input('nome'),
                'ativo' => $this->parseBoolean($request->input('ativo'))
            );

            $this->conjuntoRepository->update($id, $params);
            return $this->responseJsonMessage('Atualizado com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function destroy(int $id)
    {
        try
        {
            if(empty($this->conjuntoRepository->get($id)))
                return $this->responseError('Conjunto não encontrado', 404);

            $this->conjuntoRepository->destroy($id);
            return $this->responseJsonMessage('Excluido com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }
}
