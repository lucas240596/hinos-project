<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Repositories\HinoRepository;
use App\Validators\HinoValidator;
use Illuminate\Http\Request;
use Exception;

class HinoService extends Services
{
    private $hinoRepository;

    public function __construct()
    {
        $this->hinoRepository = new HinoRepository();
    }

    public function getAll(Request $request)
    {
        try
        {
            $params = [];
            if($request->has('cantor_id'))
                array_push($params, [0 =>'cantor_id', 1 => $request->input('cantor_id')]);
            if($request->has('conjunto_id'))
                array_push($params, [0 => 'conjunto_id', 1 => $request->input('conjunto_id')]);
            if($request->has('is_playback'))
                array_push($params, [0 => 'is_playback', 1 => $this->parseBoolean($request->input('is_playback'))]);

            $hinos = $this->hinoRepository->getAll($params);
            return $this->responseJsonData($hinos);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function get(int $id)
    {
        try
        {
            $hino = $this->hinoRepository->get($id);
            return $this->responseJsonData($hino);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function create(Request $request)
    {
        $validator = $this->validate($request->all(), HinoValidator::CREATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            $params = array(
                'nome'          => $request->input('nome'),
                'arquivo'       => Storage::disk('public')->put(STORAGE_HINOS, $request->file('arquivo')),
                'is_playback'   => $this->parseBoolean($request->input('is_playback')),
                'cantor_id'     => $request->input('cantor_id'),
                'conjunto_id'   => $request->input('conjunto_id')
            );

            $this->hinoRepository->create($params);
            return $this->responseJsonMessage('Cadastrado com sucesso', 201);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function update(int $id, Request $request)
    {
        $validator = $this->validate($request->all(), HinoValidator::UPDATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            $hino = $this->hinoRepository->get($id);
            if(empty($hino))
                return $this->responseError('Hino não encontrado', 404);

            $params = array(
                'nome'          => $request->input('nome'),
                'cantor_id'     => $request->input('cantor_id'),
                'conjunto_id'   => $request->input('conjunto_id'),
                'is_playback'   => $this->parseBoolean($request->input('is_playback')),
                'ativo'         => $this->parseBoolean($request->input('ativo'))
            );

            if($request->has('arquivo')) {
                Storage::disk('public')->delete($hino->arquivo);
                $params['arquivo'] = Storage::disk('public')->put(STORAGE_HINOS, $request->file('arquivo'));
            }

            $this->hinoRepository->update($id, $params);
            return $this->responseJsonMessage('Atualizado com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function destroy(int $id)
    {
        try
        {
            $hino = $this->hinoRepository->get($id);
            if(empty($hino))
                return $this->responseError('Hino não encontrado', 404);

            Storage::disk('public')->delete($hino->arquivo);
            $this->hinoRepository->destroy($id);
            return $this->responseJsonMessage('Excluido com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }
}
