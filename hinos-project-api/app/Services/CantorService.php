<?php

namespace App\Services;

use App\Repositories\CantorRepository;
use App\Validators\CantorValidator;
use Illuminate\Http\Request;
use Exception;

class CantorService extends Services
{
    private $cantorRepository;

    public function __construct()
    {
        $this->cantorRepository = new CantorRepository();
    }

    public function getAll()
    {
        try
        {
            $cantores = $this->cantorRepository->getAll();
            return $this->responseJsonData($cantores);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function get(int $id)
    {
        try
        {
            $cantor = $this->cantorRepository->get($id);
            return $this->responseJsonData($cantor);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function create(Request $request)
    {
        $validator = $this->validate($request->all(), CantorValidator::CREATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            if($this->cantorRepository->getByName($request->input('nome')))
                return $this->responseError('Cantor já existe', 409);

            $params = array(
                'nome' => $request->input('nome')
            );

            $this->cantorRepository->create($params);
            return $this->responseJsonMessage('Cadastrado com sucesso', 201);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function update(int $id, Request $request)
    {
        $validator = $this->validate($request->all(), CantorValidator::UPDATE_RULES);
        if($validator) return $this->responseJsonData($validator, 400);

        try
        {
            if(empty($this->cantorRepository->get($id)))
                return $this->responseError('Cantor não encontrado', 404);

            $params = array(
                'nome' => $request->input('nome'),
                'ativo' => $this->parseBoolean($request->input('ativo'))
            );

            $this->cantorRepository->update($id, $params);
            return $this->responseJsonMessage('Atualizado com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function destroy(int $id)
    {
        try
        {
            if(empty($this->cantorRepository->get($id)))
                return $this->responseError('Cantor não encontrado', 404);

            $this->cantorRepository->destroy($id);
            return $this->responseJsonMessage('Excluido com sucesso');
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }
}
