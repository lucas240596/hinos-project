<?php

namespace App\Services;

use App\Repositories\CantorRepository;
use App\Repositories\ConjuntoRepository;
use App\Repositories\HinoRepository;
use Exception;

class DashboardService extends Services
{
    private $cantorRepository;
    private $conjuntoRepository;
    private $hinoRepository;

    public function __construct()
    {
        $this->cantorRepository = new CantorRepository();
        $this->conjuntoRepository = new ConjuntoRepository();
        $this->hinoRepository = new HinoRepository();
    }

    public function countCantor() {
        try
        {
            $count = $this->cantorRepository->count();
            return $this->responseJsonData($count);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function countConjunto() {
        try
        {
            $count = $this->conjuntoRepository->count();
            return $this->responseJsonData($count);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }

    public function countHino() {
        try
        {
            $count = $this->hinoRepository->count();
            return $this->responseJsonData($count);
        }
        catch(Exception $e)
        {
            return $this->responseError($e->getMessage());
        }
    }
}
