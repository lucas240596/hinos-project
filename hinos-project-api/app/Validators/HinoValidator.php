<?php

namespace App\Validators;

class HinoValidator
{
    const CREATE_RULES = [
        'nome'          => 'required',
        'arquivo'       => 'required',
        'is_playback'   => 'required',
        'cantor_id'     => 'required',
        'conjunto_id'   => 'required'
    ];

    const UPDATE_RULES = [
        'nome'          => 'required',
        'cantor_id'     => 'required',
        'conjunto_id'   => 'required',
        'is_playback'   => 'required',
        'ativo'         => 'required'
    ];
}
