<?php

namespace App\Validators;

class CantorValidator
{
    const CREATE_RULES = [
        'nome' => 'required'
    ];

    const UPDATE_RULES = [
        'nome' => 'required',
        'ativo' => 'required'
    ];
}
