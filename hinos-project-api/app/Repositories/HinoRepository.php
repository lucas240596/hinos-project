<?php

namespace App\Repositories;

use App\Interfaces\HinoRepositoryInterface;
use App\Models\Hinos;

class HinoRepository implements HinoRepositoryInterface
{
    private $model;

    public function __construct()
    {
        $this->model = new Hinos();
    }

    public function getAll(array $params)
    {
        $query = $this->model->query();

        foreach($params as $param) {
            $query = $query->where($param[0], $param[1]);
        }

        $hinos = $query->orderBy('nome')->get();

        foreach($hinos as $hino) {
            $hino->cantor;
            $hino->conjunto;
        }
        return $hinos;
    }

    public function get(int $id)
    {
        return $this->model->find($id);
    }

    public function create(array $params)
    {
        return $this->model->create($params);
    }

    public function update(int $id, array $params)
    {
        return $this->model->find($id)->update($params);
    }

    public function destroy(int $id)
    {
        return $this->model->find($id)->delete();
    }

    public function count()
    {
        return $this->model->count();
    }
}
