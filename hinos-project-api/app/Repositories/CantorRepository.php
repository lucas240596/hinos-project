<?php

namespace App\Repositories;

use App\Interfaces\CantorRepositoryInterface;
use App\Models\Cantores;

class CantorRepository implements CantorRepositoryInterface
{
    private $model;

    public function __construct()
    {
        $this->model = new Cantores();
    }

    public function getAll()
    {
        return $this->model->orderBy('nome')->get();
    }

    public function get(int $id)
    {
        return $this->model->find($id);
    }

    public function getByName(string $nome)
    {
        return $this->model->where('nome', $nome)->first();
    }

    public function create(array $params)
    {
        return $this->model->create($params);
    }

    public function update(int $id, array $params)
    {
        return $this->model->find($id)->update($params);
    }

    public function destroy(int $id)
    {
        return $this->model->find($id)->delete();
    }

    public function count()
    {
        return $this->model->count();
    }
}
