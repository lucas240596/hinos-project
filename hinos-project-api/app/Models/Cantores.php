<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cantores extends Model
{
    protected $table = 'cantores';

    protected $fillable = [
        'id',
        'nome',
        'ativo'
    ];

    public function hinos() {
        return $this->hasMany('App\Models\Hinos', 'cantor_id', 'id');
    }
}
