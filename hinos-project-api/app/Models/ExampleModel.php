<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExampleModel extends Model
{
    protected $table = 'schema.nome_tabela';

    protected $fillable = [
        'campos'
    ];
}
