<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conjuntos extends Model
{
    protected $table = 'conjuntos';

    protected $fillable = [
        'id',
        'nome',
        'ativo',
    ];

    public function hinos() {
        return $this->hasMany('App\Models\Hinos', 'conjunto_id', 'id');
    }
}
