<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hinos extends Model
{
    protected $table = 'hinos';

    protected $fillable = [
        'id',
        'nome',
        'arquivo',
        'is_playback',
        'ativo',
        'cantor_id',
        'conjunto_id'
    ];

    protected $appends = [
        'arquivo_path'
    ];

    public function cantor() {
        return $this->belongsTo('App\Models\Cantores', 'cantor_id', 'id');
    }

    public function conjunto() {
        return $this->belongsTo('App\Models\Conjuntos', 'conjunto_id', 'id');
    }

    public function getArquivoPathAttribute() {
        return env('APP_URL').'/storage' . '/' . $this->attributes['arquivo'];
    }
}
