<?php

namespace App\Http\Controllers;

use App\Services\CantorService;
use Illuminate\Http\Request;

class CantorController extends Controller
{
    private $cantorService;

    public function __construct()
    {
        $this->cantorService = new CantorService();
    }

    public function getAll()
    {
        return $this->cantorService->getAll();
    }

    public function get($id)
    {
        return $this->cantorService->get($id);
    }

    public function create(Request $request)
    {
        return $this->cantorService->create($request);
    }

    public function update($id, Request $request)
    {
        return $this->cantorService->update($id, $request);
    }

    public function destroy($id)
    {
        return $this->cantorService->destroy($id);
    }
}
