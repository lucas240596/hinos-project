<?php

namespace App\Http\Controllers;

use App\Services\ConjuntoService;
use Illuminate\Http\Request;

class ConjuntoController extends Controller
{
    private $conjuntoService;

    public function __construct()
    {
        $this->conjuntoService = new ConjuntoService();
    }

    public function getAll()
    {
        return $this->conjuntoService->getAll();
    }

    public function get($id)
    {
        return $this->conjuntoService->get($id);
    }

    public function create(Request $request)
    {
        return $this->conjuntoService->create($request);
    }

    public function update($id, Request $request)
    {
        return $this->conjuntoService->update($id, $request);
    }

    public function destroy($id)
    {
        return $this->conjuntoService->destroy($id);
    }
}
