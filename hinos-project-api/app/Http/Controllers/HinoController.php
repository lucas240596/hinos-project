<?php

namespace App\Http\Controllers;

use App\Services\HinoService;
use Illuminate\Http\Request;

class HinoController extends Controller
{
    private $hinoService;

    public function __construct()
    {
        $this->hinoService = new HinoService();
    }

    public function getAll(Request $request)
    {
        return $this->hinoService->getAll($request);
    }

    public function get($id)
    {
        return $this->hinoService->get($id);
    }

    public function create(Request $request)
    {
        return $this->hinoService->create($request);
    }

    public function update($id, Request $request)
    {
        return $this->hinoService->update($id, $request);
    }

    public function destroy($id)
    {
        return $this->hinoService->destroy($id);
    }
}
