<?php

namespace App\Http\Controllers;

use App\Services\DashboardService;

class DashboardController extends Controller
{
    private $dashboardService;

    public function __construct()
    {
        $this->dashboardService = new DashboardService();
    }

    public function cantor()
    {
        return $this->dashboardService->countCantor();
    }

    public function conjunto()
    {
        return $this->dashboardService->countConjunto();
    }

    public function hino()
    {
        return $this->dashboardService->countHino();
    }
}
