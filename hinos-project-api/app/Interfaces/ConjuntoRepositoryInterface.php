<?php

namespace App\Interfaces;

interface ConjuntoRepositoryInterface
{
    public function getAll();
    public function get(int $id);
    public function getByName(string $nome);
    public function create(array $params);
    public function update(int $id, array $params);
    public function destroy(int $id);
    public function count();
}
