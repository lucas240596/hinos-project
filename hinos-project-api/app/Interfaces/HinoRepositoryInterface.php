<?php

namespace App\Interfaces;

interface HinoRepositoryInterface
{
    public function getAll(array $params);
    public function get(int $id);
    public function create(array $params);
    public function update(int $id, array $params);
    public function destroy(int $id);
    public function count();
}
