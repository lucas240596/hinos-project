<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'dashboard'], function () use($router) {
    $router->get('/cantor', ['uses' => 'DashboardController@cantor']);
    $router->get('/conjunto', ['uses' => 'DashboardController@conjunto']);
    $router->get('/hino', ['uses' => 'DashboardController@hino']);
});

$router->group(['prefix' => 'cantor'], function () use ($router) {
    $router->get('/',        ['uses' => 'CantorController@getAll']);
    $router->get('/{id}',    ['uses' => 'CantorController@get']);
    $router->post('/',       ['uses' => 'CantorController@create']);
    $router->put('/{id}',    ['uses' => 'CantorController@update']);
    $router->delete('/{id}', ['uses' => 'CantorController@destroy']);
});

$router->group(['prefix' => 'hino'], function () use ($router) {
    $router->get('/',        ['uses' => 'HinoController@getAll']);
    $router->get('/{id}',    ['uses' => 'HinoController@get']);
    $router->post('/',       ['uses' => 'HinoController@create']);
    $router->post('/{id}',    ['uses' => 'HinoController@update']);
    $router->delete('/{id}', ['uses' => 'HinoController@destroy']);
});

$router->group(['prefix' => 'conjunto'], function () use ($router) {
    $router->get('/',        ['uses' => 'ConjuntoController@getAll']);
    $router->get('/{id}',    ['uses' => 'ConjuntoController@get']);
    $router->post('/',       ['uses' => 'ConjuntoController@create']);
    $router->put('/{id}',    ['uses' => 'ConjuntoController@update']);
    $router->delete('/{id}', ['uses' => 'ConjuntoController@destroy']);
});
