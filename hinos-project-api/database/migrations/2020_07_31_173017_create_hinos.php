<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHinos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hinos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('arquivo');
            $table->boolean('is_playback')->default(false);
            $table->boolean('ativo')->default(true);
            $table->unsignedBigInteger('cantor_id');
            $table->unsignedBigInteger('conjunto_id');
            $table->foreign('cantor_id')->references('id')->on('cantores');
            $table->foreign('conjunto_id')->references('id')->on('conjuntos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hinos');
    }
}
