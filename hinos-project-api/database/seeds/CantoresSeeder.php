<?php

use Illuminate\Database\Seeder;
use App\Repositories\CantorRepository;

class CantoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cantorRepository = new CantorRepository();

        $cantores = [
            'Desconhecido',
            'Aline Barros',
            'Cassiane',
            'Fernanda Brum',
            'Damares',
            'Fernandinho',
            'Eyshila',
            'Thalles Roberto',
            'André Valadão',
            'Kleber Lucas',
            'Irmão Lázaro',
            'Rose Nascimento',
            'Ludmila Ferber',
            'Lauriete',
            'Shirley Carvalhaes',
            'Pricilla Alcantara',
            'Soraya Moraes',
            'Ana Paula Valadão',
            'Cristina Mel',
            'Mattos Nascimento',
            'Regis Danese',
            'Juliano Son',
            'Daniela Araújo',
            'Ton Carfi',
            'Mariana Valadão',
            'Marquinhos Gomes',
            'Elaine de Jesus',
            'Jamily',
            'Paulo César Baruk',
            'Mara Lima',
            'Dadiv Quinlan',
            'Mariana de Oliveira',
            'Jotta A',
            'Mara Maravilha',
            'Chris Durán',
            'Michael W. Smith',
            'Andrea Fontes',
            'Marcelo Nascimento',
            'Isadora Pompeo',
            'Sérgio Lopes',
            'Jairinho Manhães',
            'Vanilda Bordieri',
            'Lydia Moisés',
            'Elias Silva'
        ];

        foreach($cantores as $nome)
        {
            $params = array('nome' => $nome);
            $cantorRepository->create($params);
        }
    }
}
