export default {

  required: v => {
    return (v && v.toString().length > 0) || 'Por favor, preencha este campo'
  }

}
