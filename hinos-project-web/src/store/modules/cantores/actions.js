import http from '@/http'

export default {
  ActionSetCantores({ commit }, payload) {
    commit('setCantores', payload)
  },

  async ActionLoadCantores({ dispatch }) {
    try
    {
      let response = await http.get('cantor')
      dispatch('ActionSetCantores', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionSaveCantor({ dispatch }, params) {
    try
    {
      let response = await http.post('cantor', params)
      dispatch('ActionLoadCantores')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionUpdateCantor({ dispatch }, params) {
    try
    {
      let response = await http.put(`cantor/${params.id}`, params)
      dispatch('ActionLoadCantores')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionDestroyCantor({ dispatch }, id) {
    try
    {
      let response = await http.delete(`cantor/${id}`)
      dispatch('ActionLoadCantores')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  }
}
