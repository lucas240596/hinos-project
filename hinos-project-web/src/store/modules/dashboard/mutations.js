export default {
  setCountCantores(state, payload) {
    state.countCantores = payload
  },

  setCountConjuntos(state, payload) {
    state.countConjuntos = payload
  },

  setCountHinos(state, payload) {
    state.countHinos = payload
  },
}
