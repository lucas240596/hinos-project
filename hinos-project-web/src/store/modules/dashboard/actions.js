import http from '@/http'

export default {
  ActionSetCountCantores({ commit }, payload) {
    commit('setCountCantores', payload)
  },

  ActionSetCountConjuntos({ commit }, payload) {
    commit('setCountConjuntos', payload)
  },

  ActionSetCountHinos({ commit }, payload) {
    commit('setCountHinos', payload)
  },

  async ActionGetCountCantores({ dispatch }) {
    try
    {
      let response = await http.get('dashboard/cantor')
      dispatch('ActionSetCountCantores', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionGetCountConjuntos({ dispatch }) {
    try
    {
      let response = await http.get('dashboard/conjunto')
      dispatch('ActionSetCountConjuntos', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionGetCountHinos({ dispatch }) {
    try
    {
      let response = await http.get('dashboard/hino')
      dispatch('ActionSetCountHinos', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  }
}
