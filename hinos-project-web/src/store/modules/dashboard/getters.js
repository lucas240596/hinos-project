export default {
  getCountCantores({ countCantores }) {
    return countCantores
  },

  getCountConjuntos({ countConjuntos }) {
    return countConjuntos 
  },

  getCountHinos({ countHinos }) {
    return countHinos 
  },
}
