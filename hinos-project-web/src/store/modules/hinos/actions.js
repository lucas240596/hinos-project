import http from '@/http'

export default {
  ActionSetHinos({ commit }, payload) {
    commit('setHinos', payload)
  },

  async ActionLoadHinos({ dispatch }, params) {
    try
    {
      let response = await http.get('hino', { params })
      dispatch('ActionSetHinos', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionSaveHino({ dispatch }, params) {
    try
    {
      let response = await http.post('hino', params)
      dispatch('ActionLoadHinos', [])
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionUpdateHino({ dispatch }, params) {
    try
    {
      let response = await http.post(`hino/${params.get('id')}`, params)
      dispatch('ActionLoadHinos', [])
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionDestroyHino({ dispatch }, id) {
    try
    {
      let response = await http.delete(`hino/${id}`)
      dispatch('ActionLoadHinos', [])
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  }
}
