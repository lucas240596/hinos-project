import http from '@/http'

export default {
  ActionSetConjuntos({ commit }, payload) {
    commit('setConjuntos', payload)
  },

  async ActionLoadConjuntos({ dispatch }) {
    try
    {
      let response = await http.get('conjunto')
      dispatch('ActionSetConjuntos', response.data)
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionSaveConjunto({ dispatch }, params) {
    try
    {
      let response = await http.post('conjunto', params)
      dispatch('ActionLoadConjuntos')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionUpdateConjunto({ dispatch }, params) {
    try
    {
      let response = await http.put(`conjunto/${params.id}`, params)
      dispatch('ActionLoadConjuntos')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  },

  async ActionDestroyConjunto({ dispatch }, id) {
    try
    {
      let response = await http.delete(`conjunto/${id}`)
      dispatch('ActionLoadConjuntos')
      return Promise.resolve(response)
    }
    catch(error)
    {
      return Promise.reject(error)
    }
  }
}
