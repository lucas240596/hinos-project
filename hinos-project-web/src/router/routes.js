export default [
  {
    path: '*',
    redirect: '/'
  },
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('@/views/Dashboard')
  },{
    path: '/cantores',
    name: 'Cantores',
    component: () => import('@/views/Cantores')
  },
  {
    path: '/conjuntos',
    name: 'Conjuntos',
    component: () => import('@/views/Conjuntos')
  },
  {
    path: '/hinos',
    name: 'Hinos',
    component: () => import('@/views/Hinos')
  },
]
