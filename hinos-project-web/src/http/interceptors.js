export const response = response => {
  return response
}

export const error = error => {
  if(!error.response) {
    error.response = {
      data: { message: 'Erro inesperado' }
    }
  }

  return Promise.reject(error)
}
